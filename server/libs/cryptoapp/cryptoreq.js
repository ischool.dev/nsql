/**
 * @file cryptoreq.js contains function needed for cryptoapp connection.
 * @module cryptoreq
 * @author Quentin DIEPPE 
 */

/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* npm modules */
const axios = require('axios');

/* Local modules */
const { irsa, nodersa, encrypt, decrypt, rescrypt, resdecrypt } = require('./crypto.js');

/**
 * Load RSA pubkeys from server.
 * @param {string} url - Url of the cryptoapp server.
 */
async function rsa(url) { return (await axios.post(url + "/cryptoapp/rsa", {})).data.map(rsa => irsa(rsa)); }

/**
 * Connect to your cryptoapp server.
 * @param {string} url - Url of the cryptoapp server.
 */
async function cryptoreq(url) {
	var RSAs = await rsa(url);
	return ({
		RSAs,
		emit : (async (endpoint, data) => {
			var encrypted = encrypt(RSAs, data);
			return (resdecrypt(encrypted.keys, (await axios({ 
				method: "post",
				url : endpoint,
				data: encrypted.data.toString('base64'),
				headers: { "content-type" : "application/x-www-form-urlencoded;charset=utf-8" }
			})).data));
		}) 
	});
}

module.exports = cryptoreq;
