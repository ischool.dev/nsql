/**
 * @file crypto.js contains cryptogaphic functions needed by cryptoapp.
 * @module crypto
 * @author Quentin DIEPPE
 */

/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* npm modules */
const random = require('crypto-secure-random-digit').randomDigits;
const randomstring = require('randomstring').generate;
const { Keccak } = require('sha3');
const { hash } = require('argon2');

/* Local modules */
const serpent = require('./serpent.js');

/* Modified npm modules */
const nodersa = (size) => require('node-rsa')({ b : size });
const irsa = (rsa) => require('node-rsa')(rsa);

/* Number Of CBCs */
const cbc = 7;

/* THIS SECTION CONCERN ENCRYPTION OF DATAS */

/**
 * Generate a key of length size.
 * @param {number} size - size of the key (256bits/384bits/512bits)
 */

function keygen(size) {
	const iv = [...random(size / 8)];
	const mv = iv.map(e => [...random(e * 32)])
	const key = mv.map(e => e.reduce((ac, x) =>
		((Math.floor(Math.random() * (x + Math.random()) * 256) + (ac * (x + Math.random()))) % 256),
		Math.floor(Math.random() * 256)));
	return (Buffer.from(key).toString("hex"));
}

function encryptkey(pubkeys, key) {
	var ckey = Buffer.from(key, "hex");
	for (var i = 0; i < pubkeys.length; i++) ckey = pubkeys[i].encrypt(ckey);
	return (ckey);
}

function encryptkeys(pubkeys, keys) {
	var rsas = pubkeys;
	return (keys.map((key) => encryptkey(rsas, key).toString("hex")));
}

/**
 * Encrypt Data
 * @param {array} pubkeys - Array of RSA pubkeys.
 * @param {object} data - JS Object you want to encrypt.
 */
function encrypt(pubkeys, data) {
	const keys = [
		keygen(384), keygen(384), keygen(384), keygen(384),
	];
	const keys_ = [...keys];

	const encryptedkeys = encryptkeys(pubkeys, keys);
	const cdata = serpent.encrypt(keys, Buffer.from(JSON.stringify(data)), cbc).toString('hex');
	return ({ keys : keys_, data : Buffer.from(JSON.stringify({encryptedkeys,cdata})) });
}

function rescrypt(privkeys, data, response) {
	const odata = JSON.parse(data.toString())
	const { encryptedkeys } = odata;

	/* decrypt keys */
	var keys = decryptkeys(privkeys, encryptedkeys);
	const cdata = serpent.encrypt(keys, Buffer.from(JSON.stringify(response)), cbc);
	return (cdata); 
}

/* THIS SECTION CONCERN DECRYPTION OF DATAS */

function decryptkey(privkeys, key) {
	var ckey = Buffer.from(key, "hex");
	for (var i = (privkeys.length - 1); i > -1; i--) ckey = privkeys[i].decrypt(ckey);
	return (ckey);
}

function decryptkeys(privkeys, keys) {
	var rsas = privkeys;
	return (keys.map((key) => decryptkey(rsas, key).toString("hex")));
}

/**
 * Decrypt Data
 * @param {array} privkeys - Array of RSA privkeys.
 * @param {object} data - JS Object you want to decrypt.
 */
function decrypt(privkeys, data) {
	const odata = JSON.parse(data.toString())
	const { encryptedkeys, cdata } = odata;

	/* decrypt keys */
	var keys = decryptkeys(privkeys, encryptedkeys);

	/* rotate keys */
	for (var i = 0; i < cbc; i++) { var key = keys.shift(); keys = keys.concat(key); } 

	const data_ = serpent.decrypt(keys, Buffer.from(cdata, "hex"), cbc).toString();
	const idata = JSON.parse(data_.replace(/[^\x20-\x7E]/g, ""));
	return (idata);
}

function resdecrypt(keys, response) {
	response = Buffer.from(response, 'base64');
	
	/* rotate keys */
	for (var i = 0; i < cbc; i++) { var key = keys.shift(); keys = keys.concat(key); } 

	const data_ = serpent.decrypt(keys, response, cbc).toString();
	const idata = JSON.parse(data_.replace(/[^\x20-\x7E]/g, ""));
	return (idata);
}

module.exports = {
	irsa, nodersa,
	encrypt, decrypt,
	rescrypt, resdecrypt
}
