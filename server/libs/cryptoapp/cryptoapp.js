/**
 * @file cryptoapp.js contains function for instantiating cryptoapp.
 * @module cryptoapp
 * @author Quentin DIEPPE
 */

/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* npm modules */
var bodyParser = require('body-parser');

/* Cryptographic tools. */
const { nodersa, encrypt, decrypt, rescrypt, resdecrypt } = require('./crypto.js');

/**
 * Instantiate cryptoapp from express app.
 * @param {object} app - app generated from express.
 */
function cryptoapp(app) {
	var app_ = app;
	app_.use(bodyParser.urlencoded({ extended: false, limit : '5000mb' }));

	/* Generating RSA keys. */
	var rsas = [nodersa(768),nodersa(1024),nodersa(1536),nodersa(2048)];

	/* Endpoint for client requesting RSA pubkeys. */
	app.post("/cryptoapp/rsa", (req, res) => {
		res.json(rsas.map(rsa => rsa.exportKey("pkcs8-public-pem")));
	});

	return ({
		app_,
		rsas,
		on : ((endpoint, callback) => {
				/* express endpoint */
				app_.post(endpoint, async (req, res) => {
					try {
						/* Receive encrypted datas (encoded base64 after cryptography). */
						const encrypted = Buffer.from(Object.keys(req.body)[0], 'base64');
						
						/* Decrypt datas as JS Object. */
						const data = decrypt(rsas, encrypted);

						/* Execute the callback with decrypted datas, then encrypt the response. */
						const response = rescrypt(rsas, encrypted, await callback(data)).toString('base64');

						/* Send the encrypted response. */
						res.send(response);
					} catch(err) { res.status(400).json({ message : "Bad Request." }); }
				})
		})
	});
}

module.exports =  cryptoapp;
