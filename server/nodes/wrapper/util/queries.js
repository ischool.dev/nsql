/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* Native modules */
var util = require('util');

/* npm modules */
const { SHA3, Keccak, SHAKE } = require('sha3');

/* Local modules */
const { hash, integrityNumbers, imax } = require('./integrity');
const { loadDBS } = require('./load');

async function execute(key, query) {
    try {
	var dbs = await loadDBS(key);
    	var mysql = require('mysql');

	/* Execute Queries (SQL Protocol) */
        var responses = await Promise.all(dbs.map(async (db) => {
	    var connection = await mysql.createConnection(db);
	    
	    connection.execute = util.promisify(connection.query);
	    connection.connect = util.promisify(connection.connect);
            connection.end = util.promisify(connection.end);
	    
	    await connection.connect();
	    var response = (await connection.execute(query));
	    await connection.end();
            return (response);
        }));

	/* Create Responses' Hashes */
        var integrities = responses.map((e) => {
            var estr = JSON.stringify(e);
            return ({
                sha3: {
                    sha3_256bits: hash(new SHA3(256), estr),
                    sha3_384bits: hash(new SHA3(384), estr),
                    sha3_512bits: hash(new SHA3(512), estr),
                },
                keccak: {
                    keccak_256bits: hash(new Keccak(256), estr),
                    keccak_384bits: hash(new Keccak(384), estr),
                    keccak_512bits: hash(new Keccak(512), estr),
                },
                shake: {
                    shake_128Bits: hash(new SHAKE(128), estr),
                    shake_256bits: hash(new SHAKE(256), estr),
                }
            });
        })

	/* Return most answered response */
        var inums = integrityNumbers(integrities);
        return (responses[imax(inums)]);
    } catch (err) { return ({ message: 'Invalid SQL Request.' }); }
}

module.exports = async (key, query) => {
    return (await execute(key, query));
}
