/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

function hash(algo, string) {
    algo.update(string);
    return (algo.digest('hex'));
}

function compareIntegrity(i0, i1) {
    var i0str = JSON.stringify(i0);
    var i1str = JSON.stringify(i1);
    return (i0str == i1str);
}

function compareIntegrityArray(i, iarr) {
    var it = []
    for (var x = 0 in iarr) {
        it.push(compareIntegrity(i, iarr[x]))
    }
    var reducer = (ac, cv) => ac + cv;
    return (it.reduce(reducer, 0));
}

function integrityNumbers(arr) {
    inums = []
    for (var i = 0 in arr) {
        var arr2splice = [...arr];
        var integrity = arr2splice.splice(i, 1)[0];
        inums.push(compareIntegrityArray(integrity, arr2splice));
    }
    return (inums);
}

function imax(inums) {
    var reducer = (ac, cv) => (cv > ac) ? cv : ac;
    return (inums.reduce(reducer, 0));
}

module.exports = { hash, integrityNumbers, imax }
