/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* native npm modules */
const fs = require("fs");

/* Keccak algorithm */
const { sha512 } = require("js-sha512");

/* mcrypt.so library */
const mcrypt = require('mcrypt').MCrypt;

/* Serpent-128bits Block Cipher*/
var serpent = new mcrypt('serpent', 'cbc');
var rijndael = new mcrypt('rijndael-256', 'cbc');

const hash = (msg) => sha512(msg);

function keying(username, password, size) {
	const key = hash(Buffer.from(
			hash(username) + hash(password)
		, "hex")
		.toString("base64"));
	const buffer = Buffer.from(key, "hex");
	return ({ key: buffer.subarray(0, 32), iv: buffer.subarray(32, (size / 8)) });
}

function open(key_) {
	const { key, iv } = key_;
	serpent.open(key, iv);
}

function encrypt(username, password, filename, data) {
	const k0 = keying(username, password, 384);
	const k1 = keying(hash(username), hash(password), 384);
	const k2 = keying(username + k0.iv, password + k1.iv, 384);
	const k3 = keying(hash(k0.iv) + hash(username), hash(k1.iv) + hash(password), 384);
	const k4 = keying(hash(k0.iv) + k2.iv, hash(k1.iv) + k3.iv, 384);
	const k5 = keying(hash(k4.iv) + username, hash(k3.iv) + password, 384);
	const k6 = keying(hash(k5.iv) + password, hash(k4.iv) + username, 384);

	open(k0); const x0 = serpent.encrypt(JSON.stringify(data));
	open(k1); const x1 = serpent.encrypt(x0);
	open(k2); const x2 = serpent.encrypt(x1);
	open(k3); const x3 = serpent.encrypt(x2);
	open(k4); const x4 = serpent.encrypt(x3);
	open(k5); const x5 = serpent.encrypt(x4);
	open(k6); const x6 = serpent.encrypt(x5);

	fs.writeFileSync(filename, x6);
	return (x6);
}

function decrypt(username, password, filename) {
	const k0 = keying(username, password, 384);
	const k1 = keying(hash(username), hash(password), 384);
	const k2 = keying(username + k0.iv, password + k1.iv, 384);
	const k3 = keying(hash(k0.iv) + hash(username), hash(k1.iv) + hash(password), 384);
	const k4 = keying(hash(k0.iv) + k2.iv, hash(k1.iv) + k3.iv, 384);
	const k5 = keying(hash(k4.iv) + username, hash(k3.iv) + password, 384);
	const k6 = keying(hash(k5.iv) + password, hash(k4.iv) + username, 384);
	
	const data = fs.readFileSync(filename);

	open(k6); const x6 = serpent.decrypt(data);
	open(k5); const x5 = serpent.decrypt(x6);
	open(k4); const x4 = serpent.decrypt(x5);
	open(k3); const x3 = serpent.decrypt(x4);
	open(k2); const x2 = serpent.decrypt(x3);
	open(k1); const x1 = serpent.decrypt(x2);
	open(k0); const x0 = serpent.decrypt(x1);

	return (JSON.parse(x0
		.toString().replace(/[^\x20-\x7E]/g, '')));
}

module.exports = (key) => {
	const username = key.substring(0, 32);
	const password = key.substring(32, 64);
	return ({
		username, password,
		keying: (size) => keying(username, password),
		encrypt: (filename, data) => encrypt(username, password, filename, data),
		decrypt: (filename) => decrypt(username, password, filename)
	});
}
