/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/*
 ****************************************
 * THIS FILE IS UNUSED FOR NOW.         *
 * FEEL FREE TO IMPLEMENT HISTORY NODE. *
 ****************************************
 */

const url = (host, port) => ("http://" + host + ":" + port);

module.exports = async function history(history) {
	try {
		const { host, port, username, password } = history;
		var cryptorequest = await require('../cryptoapp/cryptoreq.js')(url(host, port));
		var token = await cryptorequest.emit(url(host, port) + "/nsql/access", { username, password });
		return ({ status : true, token, cryptorequest,
			list : async (hist, from, to) => (await cryptorequest.emit(url(host, port) + "/nsql/list", { token, hist, from, to })),
			deploy : async (hist, from, to, db) => (await cryptorequest.emit(url(host, port) + "/nsql/deploy", { token, hist, from, to, db }))
		});
	} catch(err) { return ({ status : false }); }
}
