/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* Native Modules */
const fs = require('fs');
var util = require('util');

/* Sha3 / Keccak / Shake */
const { SHA3, Keccak, SHAKE } = require('sha3');

/* Local Modules */
const { hash, integrityNumbers, imax } = require('./integrity');
const { loadServers } = require('./load');

const nest = JSON.parse(fs.readFileSync('./config/NEST'));

const DISPATCH = 0;
const WRAPPER = 1;

async function execute(key, sender, query, type) {
    try {
	const nsqlapi = await require('../../../../api/nsql-api');
	
        var dbs = await loadServers(key);
	var odbs = (type == WRAPPER) ? ([dbs[0]]) : (dbs);

	/* Execute Queries (NSQL Protocol) */
        var responses = await Promise.all(odbs.map(async (connection) => {
	    try {
		    const nsql = await nsqlapi(connection);
        	    var response = (await nsql.query(nest.id, { SQL : query }, WRAPPER));
            	    return (response);
	    } catch(error) { 
		    return ([{ message: `[ERROR] Detected on nsql-node: ${connection.host}:${connection.port}` }]) 
	    }
        }));

	/* Create Responses' Hashes */
        var integrities = responses.map((e) => {
            var estr = JSON.stringify(e);
            return ({
                sha3: {
                    sha3_256bits: hash(new SHA3(256), estr),
                    sha3_384bits: hash(new SHA3(384), estr),
                    sha3_512bits: hash(new SHA3(512), estr),
                },
                keccak: {
                    keccak_256bits: hash(new Keccak(256), estr),
                    keccak_384bits: hash(new Keccak(384), estr),
                    keccak_512bits: hash(new Keccak(512), estr),
                },
                shake: {
                    shake_128Bits: hash(new SHAKE(128), estr),
                    shake_256bits: hash(new SHAKE(256), estr),
                }
            });
        })

	/* Return most answered response */
        var inums = integrityNumbers(integrities);
        return (responses[imax(inums)]);
    } catch (err) { return ({ message: 'Invalid SQL Request.' }); }
}

module.exports = async (key, sender, query, type) => {
    return (await execute(key, sender, query, type));
}
