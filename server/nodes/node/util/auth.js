/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* Native Modules */
const fs = require('fs');

/* npm modules  */
const { Keccak } = require('sha3');
const argon2 = require('argon2');
const randomstring = require('randomstring').generate;

/* Configuration file */
const { hash } = require('./integrity');
const nest = JSON.parse(fs.readFileSync('./config/NEST'));

const tokens = new Set([])

async function auth(username, password) {
    var access = "";
    var authenticated = await argon2.verify("$argon2i$v=19$m=4096,t=3,p=1$" + nest.authhash,
	    (hash(new Keccak(384), username) + hash(new Keccak(384), password)));
    if (authenticated) {
        access = randomstring(48);
        tokens.add(access);
    }
    return (access);
}

function authToken(access) {
    return (tokens.has(access));
}

module.exports = {
    auth,
    authToken
}
