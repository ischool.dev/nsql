/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

async function loadServers(key) {
	try {
		const fsmanager = require("./fsmanager")(key);
		const infosDBS = fsmanager.decrypt('./config/SQL');
		return (await Promise.all(await infosDBS.map(async (e) => 
			({
				id: e.id,
				host : e.host,
				port : e.port,
				username : e.user,
				password : e.password
			})
		)));
	} catch (err) { return ([]); }
}

module.exports = { loadServers };
