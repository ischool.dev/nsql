/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* Native modules */
const fs = require('fs');

/* npm modules */
const express = require('express')
const app = express()

/* Local modules */
const cryptoapp = require('../../libs/cryptoapp/cryptoapp.js')(app);

/* Queries API (NSQL Protocol) */
const sql = require('./util/queries');
const { auth, authToken } = require('./util/auth');

/* Configuration file */
const nest = JSON.parse(fs.readFileSync('./config/NEST'));

cryptoapp.on('/nsql/access', async (data) => {
	const { username, password } = data;
	return (await auth(username, password));
});

cryptoapp.on('/nsql/query', async (data) => {
	const { key, token, sender, query, type } = data;
	if (authToken(token)) {
		const qresp = await sql(key, sender, query, type);
		return (qresp);
	} else { return ({ message : 'Forbidden.' }) }
});

app.listen(nest.port);
