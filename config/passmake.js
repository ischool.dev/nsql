/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

const { Keccak } = require('sha3');
const argon2 = require('argon2');

const hash = (algo, string) => {
	algo.update(string);
	return (algo.digest("hex"));
} 

async function password(username, password) {
	return (await argon2.hash(hash(new Keccak(384), username) + hash(new Keccak(384), password)));
}

password(process.argv[2], process.argv[3]).then(authhash => console.log(authhash.split("p=1$")[1]));
