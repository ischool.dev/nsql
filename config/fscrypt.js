/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */


const fs = require("fs");
const { sha512 } = require("js-sha512");
const hash = (msg) => sha512(msg);

const key = hash(hash(process.argv[2]) + hash(process.argv[3]))
const fsmanager = require("../server/nodes/wrapper/util/fsmanager")(key);
const SQL = JSON.parse(fs.readFileSync("./SQL.json"));
fsmanager.encrypt("SQL", SQL);
console.log(fsmanager.decrypt("SQL"));
