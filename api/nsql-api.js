/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

const { sha512 } = require("js-sha512");
const url = (host, port) => ("http://" + host + ":" + port);

module.exports = async (uinfo) => {
    var { host, port, username, password } = uinfo;
    var cryptorequest = await require('./libs/cryptoapp/cryptoreq.js')(url(host, port));
    var token = await cryptorequest.emit(url(host, port) + "/nsql/access", { username, password });
    var key = sha512(sha512(username) + sha512(password));
    return ({
	token,
        query: (async (sender, query, type) => {
		const type_ = (type == undefined) ? 0 : type;
		return (await cryptorequest.emit(url(host, port) + "/nsql/query",
			{ key, token, sender, query : query.SQL, type: type_ }))
	})
    });
}
