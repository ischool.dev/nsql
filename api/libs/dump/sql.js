/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

const util = require('util');
var printf = require('printf');
const delta = 190;

function maxRows(keysdata_) {
	return (keysdata_.reduce((ac, cv) => (cv.rows > ac) ? ac + cv.rows : ac, 0));
}

function dumpRow(keysdata_, row) {
	var string = "|";
	for (var i = 0 in keysdata_) {
		try { row[keysdata_[i].key] = row[keysdata_[i].key].toString() } catch(err) {}
		string += printf("%-" + keysdata_[i].size + "s|", row[keysdata_[i].key].substr(0, delta));
	}
	console.log(string);
}

function dumpRows(keysdata_, rows) {
	for (var i = 0 in rows) { dumpRow(keysdata_, rows[i]); }
}

function delimiter(keysdata_) {
	var delim = "+";
	for (var i = 0 in keysdata_) {
		for (var y = 0; y < keysdata_[i].size; y++) delim += "-"; delim += "+";
	}
	return (delim);
}

function dumpKeys(keysdata_) {
	var string = "|"
	for (var i = 0 in keysdata_) { string += printf("%-" + keysdata_[i].size + "s|", keysdata_[i].key); }
	console.log(delimiter(keysdata_));
	console.log(string);
	console.log(delimiter(keysdata_));
}

function keysdata(keys, response) {
	var keysdata_ = keys.map(e => ({ key : e, size : ((e.length < delta) ? e.length : delta), rows : (e.length / delta) + 1 }));
	for (var i = 0 in response) {
		for (var y = 0 in keys) {
			if (typeof response[i][keys[y]] != "number") {
				if (response[i][keys[y]].length > keysdata_[y].size) { 
					keysdata_[y].size = (response[i][keys[y]].length <= delta) ? response[i][keys[y]].length : delta;
					keysdata_[y].rows = Math.floor(response[i][keys[y]].length / delta) + 1;
				}
			} else {
				if (response[i][keys[y]].toString().length > keysdata_[y].size) { 
					keysdata_[y].size = (response[i][keys[y]].toString().length <= delta) ? response[i][keys[y]].toString().length : delta;
					keysdata_[y].rows = Math.floor(response[i][keys[y]].toString().length / delta) + 1;
				}
			}
		}
	}
	return (keysdata_);
}

function dump(response) {
	response = (Array.isArray(response)) ? response : [response];
	var keys = Object.keys(response[0]);
	var keysdata_ = keysdata(keys, response);
	if (keysdata_.reduce((ac, cv) => ac + cv.size, 0) <= 200) {
		dumpKeys(keysdata_, response);
		dumpRows(keysdata_, response);
		console.log(delimiter(keysdata_));
		if (response[0]["message"] != "Invalid SQL Request.")
			console.log("Query OK : " + response.length + " rows.");
	} else { console.log(util.inspect(response, false, null, true));  }
}

module.exports = (response) => { try { dump(response) } catch(err) { console.log(util.inspect(response, false, null, true)); } };
