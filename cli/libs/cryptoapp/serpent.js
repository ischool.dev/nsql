/**
 * @file serpent.js contains serpent-128bits block cipher functions.
 * @module serpent
 * @author Quentin DIEPPE
 */

/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* mcrypt.so library */
const mcrypt = require('mcrypt').MCrypt;

/* Serpent-128bits Block Cipher*/
var serpent = new mcrypt('serpent', 'cbc');

/**
 * encryption.
 * @param {array} keys384 - array of 384bits key.
 * @param {buffer} bytes - bytes to encrypt.
 * @param {number} n - Number of cbc.
 */
function encrypt(keys384, bytes, n) {
    /* key384 and rotate keys */
    const key384 = keys384[0]; var k384 = keys384.shift(); keys384 = keys384.concat(k384);

    /* key384 (384bits) = key (256bits) + iv (128bits) */
    const key = Buffer.from(key384, 'hex').subarray(0, 32);
    const iv = Buffer.from(key384, 'hex').subarray(32, 48);

    /* encryption */
    serpent.open(key, iv);
    const encrypted = serpent.encrypt(bytes);

    /* n recursive */
    if (n - 1) { return (encrypt(keys384, Buffer.from(encrypted), n - 1)); }
    else { return (Buffer.from(encrypted)); }
}

/**
 * decryption.
 * @param {array} keys384 - array of 384bits key.
 * @param {buffer} bytes - bytes to encrypt.
 * @param {number} n - Number of cbc.
 */
function decrypt(keys384, bytes, n) {
    /* key384 and rotate keys */
    const key384 = keys384[keys384.length - 1];
    var k384 = keys384.splice(keys384.length - 1, 1); keys384 = k384.concat(keys384);

    /* key384 (384bits) = key (256bits) + iv (128bits) */
    const key = Buffer.from(key384, 'hex').subarray(0, 32);
    const iv = Buffer.from(key384, 'hex').subarray(32, 48);

    /* decryption */
    serpent.open(key, iv);
    const decrypted = serpent.decrypt(Buffer.from(bytes));

    /* n recursive */
    if (n - 1) { return (decrypt(keys384, Buffer.from(decrypted), n - 1)); }
    else { return (Buffer.from(decrypted)); }

}

module.exports = {
	encrypt,
	decrypt
}
