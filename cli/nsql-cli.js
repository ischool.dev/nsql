/**
 * @file nsql-cli.js contains nsql command-line-interface (terminal)
 * @module nsql-cli
 * @author Quentin DIEPPE
 */

/*
 *   Copyright 2020 Quentin DIEPPE
 *   
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *   	
 *   	http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

/* Native modules */
const util = require('util');

/* npm modules */
var inquirer = require('inquirer');

/* Local modules */
const dump = require('./libs/dump/sql');
const nsqlapi = require('../api/nsql-api')


/* { password } = readpassword(); */
const readpassword = async () => await inquirer.prompt([{ name: "password", type: "password", mask : "", message: "password" }]);

/* { SQL } = readquery(); */
const readquery = async () => await inquirer.prompt([{
	type: 'command', name: 'SQL', message: 'NestsSQL $>',
	autoCompletion: ['SELECT', 'INSERT', 'DROP', 'CREATE']
}]);

/**
 * Run a command-line-interface for nsql.
 * @param {object} uinfo - { host, port, username } of the nsql node (server or wrapper).
 */
async function nsqlcli(uinfo) {
	try {
		const { host, port, username, sender } = uinfo;
		var { password } = await readpassword(); console.log('\n');

		/* { token, query } = nsqlapi({ host, port, username, password }); */
		const { token, query } = await nsqlapi({ host, port, username, password });

		/* Check for authentication validation. */
		if (token == "") { console.log('bad credentials. exiting nsql-cli.'); process.exit(0); }

		/* Enable cli's history of queries */
		inquirer.registerPrompt('command', require('inquirer-command-prompt'));

		while (Infinity) {
			var { SQL } = await readquery();

			/* Special cases */
			if (SQL.trim() == "") { continue; }
			if (SQL.trim() == "exit") { process.exit(0); }

			/* Send query to nsql */
			var response = await query(sender, { SQL }, 0);

			/* Display nsql response */
			dump(response); console.log('\n');
		}
	} catch (err) { console.log('exiting nsql-cli.'); process.exit(0) }
}

/* Call nsqlcli({ host, port, username }) with process.argv (programs arguments) */
nsqlcli({ host : process.argv[2], port : process.argv[3], username : process.argv[4], sender: "nsql-cli" });
