all : install

install :
	@npm i
	@cd api && npm i
	@cd cli && npm i

clean :
	@rm -f package-lock.json
	@rm -rf node_modules
	@cd api && rm -rf node_modules && rm -f package-lock.json
	@cd cli && rm -rf node_modules && rm -f package-lock.json

deploy :
	@pm2 start server/nodes/$(server)/nsql-$(server).js --watch

run :
	@node $(tool)/nsql-$(tool).js $(host) $(port) $(user)
